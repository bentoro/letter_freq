#
# =====================================================================================
#
#       Filename:  letter_frequency.py
#
#    Description: Opens the file from arguments and prints the frequency of letters
#
#        Created:  01/20/2019
#
#         Author:  Benedict Lo
#
# =====================================================================================

import sys
from sys import argv
a, filename = argv
book = open(filename)
sys.stdout = open("results.txt", "w+")
d = {}
for i in book.read().lower():
    d[i] = d.get(i,0) + 1
for k,v in d.items():
    print("{},{}".format(k,v))
book.close()
